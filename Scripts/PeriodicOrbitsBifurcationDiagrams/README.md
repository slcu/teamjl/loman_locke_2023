### Plot periodic orbits
This script pltos periodic orbits of bifurcation diagrams. This has a specific julia environment, using the latest update to BifurcationKit (hence it is in a separate folder).
