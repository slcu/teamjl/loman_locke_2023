### Fetch Packages ###
using Plots, Plots.Measures
using StatsBase
using StatsPlots


### Basic "empty" plot ###
p0 = plot([],[],xguide="",yguide="",label="",framestyle=:none,grid=false,xticks=[],yticks=[],legend=:none)


### General Functions ###

# Reduces the decimals in a Float numer.
short_dec(num;places=3) = floor(Int64,num*10^places)/10^places
# Makes a linnear grid.
lin_grid(start,stop,n) = [range(start,stop=stop,length=n)...]
# Makes a logarithmic grid.
log_grid(start,stop,n)= 10 .^ range(log10(start),stop=log10(stop),length=n)


### Plots Basic Shapes ###

# Plots a vertical line (to e.g. mark stress addition).
plot_vertical_line(args...;kwargs...) = (plot(); plot_vertical_line!(args...;kwargs...);)
plot_vertical_line!(x_value,heigth;base=0.0,lw=4,la=1.0,linestyle=:dot,color=:red,label="",kwargs...) = plot!([x_value,x_value],[base,heigth];lw=lw,la=la,color=color,linestyle=linestyle,label=label,kwargs...)
# Plots several vertical lines.
plot_vertical_lines(args...;kwargs...) = (plot(); plot_vertical_lines!(args...;kwargs...);)
function plot_vertical_lines!(x_values,heigth;base=0.0,lw=3,la=1.0,linestyle=:dot,color=:red,label="",kwargs...)
    foreach(x_val->plot_vertical_line!(x_val,heigth;base=base,lw=lw,la=la,linestyle=linestyle,color=color,label=""), x_values)
    return plot!([],[];linestyle=linestyle,lw=lw,la=la,color=color,label=label,kwargs...)
end

# Plots a pattern of stress activation/deactivation.
plot_stress_pattern(args...;kwargs...) = (plot(); plot_stress_pattern!(args...;kwargs...);)
function plot_stress_pattern!(addition_times,removal_times,heigth;base=0.0,lw=4,la=1.0,linestyle=:dot,addition_color=:red,removal_color=:black,addition_label="",removal_label="",kwargs...)
    println("Probably remove and repalce with function: plot_vertical_lines_multi.")
    plot_vertical_lines!(addition_times,heigth;base=base,lw=lw,la=la,linestyle=linestyle,color=addition_color,label=addition_label,kwargs...)
    plot_vertical_lines!(removal_times,heigth;base=base,lw=lw,linestyle=linestyle,color=removal_color,label=removal_label,kwargs...)
end

# Plots a pattern of several types of vertical lines.
plot_vertical_lines_multi(args...;kwargs...) = (plot(); plot_vertical_lines_multi!(args...;kwargs...);)
function plot_vertical_lines_multi!(x_values,heigth;base=0.0,lw=4,lws=fill(lw,length(x_values)),la=1.0,las=fill(la,length(x_values)),linestyle=:dot,linestyles=fill(linestyle,length(x_values)),color=:red,colors=fill(color,length(x_values)),label="",labels=fill(label,length(x_values)),kwargs...)
    map(i -> plot_vertical_lines!(x_values[i],heigth;base=base,lw=lws[i],la=las[i],linestyle=linestyles[i],color=colors[i],label=labels[i],kwargs...), 1:length(x_values))
    plot!()
end


# Plots a pattern of stress activation/deactivation and induction.
function plot_stress_induction_pattern(addition_times,removal_times,induction_times,heigth)
    plot_stress_pattern(addition_times,removal_times,heigth)
    foreach(it -> plot!([it,it],[0,heigth],label="",linewidth=4,linestyle = :dot,color=:purple), induction_times)
    return plot!()
end
function plot_stress_induction_pattern!(addition_times,removal_times,induction_times,heigth)
    plot_stress_pattern!(addition_times,removal_times,heigth)
    foreach(it -> plot!([it,it],[0,heigth],label="",linewidth=4,linestyle = :dot,color=:purple), induction_times)
    return plot!()
end

# Plots a step (e.g. to mark stress levels)
plot_step(args...;kwargs...) = (plot(); plot_step!(args...;kwargs...);)
plot_step!(step_time,end_time,step_heigth;start_time=0.0,base_heigth=0.0,lw=4,la=1.0,linestyle=:solid,color=:red,label="",kwargs...) = plot!([start_time,step_time,step_time,end_time],[base_heigth,base_heigth,step_heigth,step_heigth];lw=lw,la=la,color=color,linestyle=linestyle,label=label,kwargs...)


### Plot Simulations ###

# Single activation using the RRE interpretation
function plot_detsim_activation(model::Model, pre_stress_t::Float64, post_stress_t::Float64; p_changes=[], lw=5,color=1,la=0.8,label="",xguide="Time (Hours)",yguide="[σᴮ] (µM)",ymax=Inf,activation_lw=5,activation_la=0.8,kwargs...)
    sol = detsim_activation(model,pre_stress_t,post_stress_t;p_changes=p_changes)
    plot(sol,idxs=[model.sigB_idx];lw=lw,color=color,la=la,label=label,xguide=xguide,yguide=yguide,kwargs...)
    maxY = (ymax==Inf ? 1.05*maximum(getindex.(sol.u, model.sigB_idx)) : max(ymax,1.05*maximum(getindex.(sol.u, model.sigB_idx))))
    plot_vertical_line!(0,maxY;base=-maxY/20.0,lw=activation_lw,la=activation_la,left_margin=5mm,xlimit=(-pre_stress_t,post_stress_t))
end
# Single activation using the CLE interpretation
function plot_stochsim_activation(model::Model, pre_stress_t::Float64, post_stress_t::Float64; p_changes=[], saveat=0.1, maxiters=1e6, adaptive=true, dt=0.0005, lw=3,color=1,la=0.8,label="",xguide="Time (Hours)",yguide="[σᴮ] (µM)",ymax=Inf,activation_lw=5,activation_la=0.8,
                                    sol = stochsim_activation(model,pre_stress_t,post_stress_t;p_changes=p_changes,saveat=saveat,maxiters=maxiters,adaptive=adaptive,dt=dt),kwargs...)
    plot(sol,idxs=[model.sigB_idx];lw=lw,color=color,la=la,label=label,xguide=xguide,yguide=yguide,kwargs...)
    maxY = (ymax==Inf ? 1.05*maximum(getindex.(sol.u, model.sigB_idx)) : max(ymax,1.05*maximum(getindex.(sol.u, model.sigB_idx))))
    plot_vertical_line!(0,maxY;base=-maxY/20.0,lw=activation_lw,la=activation_la,left_margin=5mm,xlimit=(-pre_stress_t,post_stress_t))
end
# Monte carlo activation using the CLE interpretation
function plot_monte_activation(model::Model, pre_stress_t::Float64, post_stress_t::Float64, n::Int64; p_changes=[], saveat=0.1, maxiters=1e6, adaptive=true, dt=0.0005, lw=3, color=nothing,la=0.8,colors=((color==nothing) ? ([1:n...]') : fill(color,1,n)),lws=fill(lw,1,n),las=fill(la,1,n),label="",xguide="Time (Hours)",yguide="[σᴮ] (µM)", ymax=Inf,  activation_lw=5,activation_la=0.8,kwargs...)
    sols = monte_activation(model,pre_stress_t,post_stress_t,n;p_changes=p_changes,saveat=saveat,maxiters=maxiters,adaptive=adaptive,dt=dt)
    plot(sols,idxs=[model.sigB_idx];lw=lws,color=colors,la=las,label=label,xguide=xguide,yguide=yguide,kwargs...)
    maxY = (ymax==Inf ? 1.05*maximum(getindex.(vcat(getfield.(sols.u,:u)...), model.sigB_idx)) : max(ymax,1.05*maximum(first.(vcat(getfield.(sols.u,:u)...)))))
    plot_vertical_line!(0,maxY;base=-maxY/20.0,lw=activation_lw,la=activation_la,left_margin=5mm,xlimit=(-pre_stress_t,post_stress_t))
end

# Single activation using the Gillespie interpretation
function plot_ssasim_activation(model::Model, pre_stress_t::Float64, post_stress_t::Float64, u0_base::Vector{Int64}, step_var::Symbol,step_val::Int64; p_changes=[], saveat=0.1, lw=3,color=1,la=0.8,xticks=[],yticks=[],label="",xguide="Time (Hours)",yguide="[σᴮ] (Molecules)",ymax=Inf,activation_lw=5,activation_la=0.8,kwargs...)
    sol = ssasim_activation(model,pre_stress_t,post_stress_t,u0_base,step_var,step_val;p_changes=p_changes,saveat=saveat)
    plot(sol,idxs=[model.sigB_idx];lw=lw,color=color,la=la,label=label,xguide=xguide,yguide=yguide,kwargs...)
    maxY = (ymax==Inf ? 1.05*maximum(getindex.(sol.u, model.sigB_idx)) : max(ymax,1.05*maximum(getindex.(sol.u, model.sigB_idx))))
    plot_vertical_line!(0,maxY;base=-maxY/20.0,lw=activation_lw,la=activation_la,left_margin=5mm,xlimit=(-pre_stress_t,post_stress_t))
end
# Monte carlo activation using the Gillespie interpretation
function plot_ssamonte_activation(model::Model, pre_stress_t::Float64, post_stress_t::Float64, u0_base::Vector{Int64}, step_var::Symbol,step_val::Int64, n::Int64; p_changes=[], saveat=0.1, lw=3, color=nothing,la=0.8,colors=((color==nothing) ? ([1:n...]') : fill(color,1,n)),lws=fill(lw,1,n),las=fill(la,1,n),label="",xguide="Time (Hours)",yguide="[σᴮ] (µM)", ymax=Inf, activation_lw=5,activation_la=0.8,kwargs...)
    sols = ssamonte_activation(model,pre_stress_t,post_stress_t,u0_base,step_var,step_val,n;p_changes=p_changes,saveat=saveat)
    plot(sols,idxs=[model.sigB_idx];lw=lws,color=colors,la=las,label=label,xguide=xguide,yguide=yguide,kwargs...)
    maxY = (ymax==Inf ? 1.05*maximum(getindex.(vcat(getfield.(sols.u,:u)...), model.sigB_idx)) : max(ymax,1.05*maximum(first.(vcat(getfield.(sols.u,:u)...)))))
    plot_vertical_line!(0,maxY;base=-maxY/20.0,lw=activation_lw,la=activation_la,left_margin=5mm,xlimit=(-pre_stress_t,post_stress_t))
end;


### Plot Bifurcation Diagram Sets ###
function plot_bif_set(bifs::Vector{BifurcationDiagrams}, idx, steady_states; model=narula_bif_model,  colors_unstab=[:pink :red :darkred], colors_stab=[:lightblue,:blue,:darkblue], lw=5,la=0.8, markercolor=:lightgreen, markersize=10, markershape=:star4, kwargs...)
    plot_bifs(bifs[idx];cUs=colors_unstab,cSs=colors_stab,lw=lw,la=la,lsU=:solid,xaxis=:log10,xticks=[model.p_vals[idx]/10.0,model.p_vals[idx],model.p_vals[idx]*10.0], kwargs...)
    scatter!([(model.p_vals[idx],steady_states[1]),(model.p_vals[idx],steady_states[2]),(model.p_vals[idx],steady_states[3])],label="",color=markercolor,markersize=markersize,markershape=markershape)
end


### Specialised Plots ###

# Plots the distinctness areas and labels them.
function make_behaviour_distinctness_area_plot(ev,f1,f2;display_data=false,set_title=true,n=1000,b_name_1="Initial Pulsing",b_name_2="Stochastic Pulsing",lw=5)
    #ev = EvaluationVector(evs,:pProd,[0.,η2,scale]);
    v_ip = f1.(ev.evaluations)
    v_sp = f2.(ev.evaluations)

    itp_ip = interpolate(v_ip, BSpline(Linear()))(range(1,stop=length(v_ip),length=n))
    itp_sp = interpolate(v_sp, BSpline(Linear()))(range(1,stop=length(v_sp),length=n))
    itp_min = min.(itp_ip,itp_sp)

    ipa = (sum(itp_ip.-itp_min))*(ev.grid[end]-ev.grid[1])/n
    spa = (sum(itp_ip.-itp_min))*(ev.grid[end]-ev.grid[1])/n
    ca = sum(itp_min)*(ev.grid[end]-ev.grid[1])/n
    if display_data
        println("$(b_name_1) Area: $(ipa)")
        println("$(b_name_2) Area: $(spa)")
        println("Combined Area: $(ca)")
        println("Distinctness Measure = $(sqrt(ipa*spa)/(ipa+spa+ca))")
    end

    plot(range(ev.grid[1],stop=ev.grid[end],length=n),itp_ip; color=4,lw=lw,fillrange=itp_min,fillalpha=0.5,label="")
    plot!(range(ev.grid[1],stop=ev.grid[end],length=n),itp_sp; color=6,lw=lw,fillrange=itp_min,fillalpha=0.5,label="")
    plot!(range(ev.grid[1],stop=ev.grid[end],length=n),itp_min; color=5,lw=0,fillrange=zeros(n),fillalpha=0.5,label="")
    plot!(xlimit=(ev.grid[1],ev.grid[end]),ylimit=(0,1.1*max(v_ip...,v_sp...)),xguide="pStress",yguide="Magnitude")
    plot!([[-1] [-1]],color=[4 6],l2=[4 4],label=["Degree of $(b_name_1)" "Degree of $(b_name_2)"])
    return plot!([[-1] [-1] [-1]],color=[4 6 5],fillrange=[[-1] [-1] [-1]],fillalpha=0.5,la=0.5,label=["Area $(b_name_1)" "Area $(b_name_2)" "Area Combined"])
end

### Figure Saving ###

# Saves a figure in four different ways (for thesis candidate figures).
function save_figure(figure,figurefolder;tag="")
    savefig(figure,"../Figures/$(figurefolder)/$(tag)generated_full.png")
    savefig(figure,"../Figures/$(figurefolder)/$(tag)generated_full.svg")
    savefig(plot!(figure;xguide="",yguide="",title="",legend=:none),"../Figures/$(figurefolder)/$(tag)inkscape_base.png")
    savefig(plot!(figure;xguide="",yguide="",title="",legend=:none),"../Figures/$(figurefolder)/$(tag)inkscape_base.svg");
    nothing
end

# Saves a figure in two different ways (for other figures).
function save_figure_minor(figure,figurefolder,figurename)
    savefig(figure,"../Figures/$(figurefolder)/$(figurename).png")
    savefig(figure,"../Figures/$(figurefolder)/$(figurename).svg")
    nothing
end