
### Basic Evaluation Plots ###

# Plots an evaluation vector.
plot_evaluation(evs::Evaluations,par::Symbol,p_base::Vector{Float64},f::Function;kwargs...) = plot_evaluation(EvaluationVector(evs,par,p_base),f;kwargs...)
plot_evaluation!(evs::Evaluations,par::Symbol,p_base::Vector{Float64},f::Function;kwargs...) = plot_evaluation!(EvaluationVector(evs,par,p_base),f;kwargs...)
plot_evaluation(args...;kwargs...) = (plot(); plot_evaluation!(args...;kwargs...);)
function plot_evaluation!(evs::EvaluationVector,f::Function;label="",dense=false,dense_dens=100,dense_alg=BSpline(Quadratic(Reflect(OnCell()))),kwargs...)
    dense && return plot!(dense_grid(evs.grid,f.(evs.evaluations),dens=dense_dens,alg=dense_alg)...;xlimit=(evs.grid[1],evs.grid[end]),label=label,xguide="$(evs.par)",kwargs...)
    return plot!(evs.grid,f.(evs.evaluations);xlimit=(evs.grid[1],evs.grid[end]),label=label,xguide="$(evs.par)",kwargs...)
end

# Plots two evaluation vectors.
plot_evaluations(evs::Evaluations,par::Symbol,p_base::Vector{Float64},f1::Function,f2::Function;kwargs...) = plot_evaluations(EvaluationVector(evs,par,p_base),f1,f2;kwargs...)
function plot_evaluations(evs::EvaluationVector,f1::Function,f2::Function;labels=["" ""],titles=["" ""],dense=false,dense_dens=100,dense_alg=BSpline(Quadratic(Reflect(OnCell()))),kwargs...)
    return plot(plot_evaluation(evs,f1;label=labels[1],title=titles[1],dense=dense,dense_dens=dense_dens,dense_alg=dense_alg),plot_evaluation(evs,f2;label=labels[2],title=titles[2],dense=dense,dense_dens=dense_dens,dense_alg=dense_alg);kwargs...)
end

# Plots an evaluation matrix.
plot_evaluation(evs::Evaluations,par1::Symbol,par2::Symbol,p_base::Vector{Float64},f::Function;kwargs...) = plot_evaluation(EvaluationMatrix(evs,par1,par2,p_base),f;kwargs...)
plot_evaluation!(evs::Evaluations,par1::Symbol,par2::Symbol,p_base::Vector{Float64},f::Function;kwargs...) = plot_evaluation!(EvaluationMatrix(evs,par1,par2,p_base),f;kwargs...)
plot_evaluation(args...;kwargs...) = (plot(); plot_evaluation!(args...;kwargs...);)
function plot_evaluation!(evs::EvaluationMatrix,f::Function;hm_min=-Inf,hm_max=Inf,legend=:right,dense=false,dense_dens=100,dense_alg=BSpline(Quadratic(Reflect(OnCell()))),kwargs...)
    dense && return heatmap!(dense_grid(evs.grid2,evs.grid1,f.(evs.evaluations),dens=dense_dens,alg=dense_alg)...;xguide="$(evs.par2)",yguide="$(evs.par1)",clim=(hm_min,hm_max),legend=legend,kwargs...)
    return heatmap!(evs.grid2,evs.grid1,f.(evs.evaluations);xguide="$(evs.par2)",yguide="$(evs.par1)",clim=(hm_min,hm_max),legend=legend,kwargs...)
end

# Plots two evaluation matrices.
plot_evaluations(evs::Evaluations,par1::Symbol,par2::Symbol,p_base::Vector{Float64},f1::Function,f2::Function;kwargs...) = plot_evaluations(EvaluationMatrix(evs,par1,par2,p_base),f1,f2;kwargs...)
function plot_evaluations(evs::EvaluationMatrix,f1::Function,f2::Function;labels=["" ""],titles=["" ""],hm_min=-Inf,hm_max=Inf,dense=false,dense_dens=100,dense_alg=BSpline(Quadratic(Reflect(OnCell()))),kwargs...)
    return plot(plot_evaluation(evs,f1;label=labels[1],title=titles[1],hm_min=hm_min,hm_max=hm_max,dense=dense,dense_dens=dense_dens,dense_alg=dense_alg),plot_evaluation(evs,f2;label=labels[2],title=titles[2],hm_min=hm_min,hm_max=hm_max,dense=dense,dense_dens=dense_dens,dense_alg=dense_alg);kwargs...)
end

# Plots a grid of evaluation plots (vector).
function plot_evaluation_grids(evs,f,par,p_base,meta_par1,meta_grid1,meta_par2,meta_grid2;hm_min=-Inf,hm_max=Inf,kwargs...)
    idx1 = get_p_idx(evs,meta_par1); idx2 = get_p_idx(evs,meta_par2);
    plots = [plot_evaluation(Eevs,par,setindex!(setindex!(deepcopy(p_base),val1,idx1),val2,idx2),f,hm_min=-Inf,hm_max=Inf) for val1 in meta_grid1, val2 in meta_grid2]
    return plot(plots...;kwargs...)
end

# Plots a grid of evaluation plots (matrix).
function plot_evaluation_grids(evs,f,par1,par2,p_base,meta_par1,meta_grid1,meta_par2,meta_grid2;hm_min=-Inf,hm_max=Inf,kwargs...)
    idx1 = get_p_idx(evs,meta_par1); idx2 = get_p_idx(evs,meta_par2);
    plots = [plot_evaluation(EvaluationMatrix(evs,par1,par2,setindex!(setindex!(deepcopy(p_base),val1,idx1),val2,idx2)),f,hm_min=-Inf,hm_max=Inf) for val1 in meta_grid1, val2 in meta_grid2]
    return plot(plots...;kwargs...)
end

# Plots an evaluation matrix, but with each point being a function over a third dimension.
function calc_evaluations(evs::Evaluations,par1::Symbol,par2::Symbol,par3::Symbol,p_base::Vector{Float64},f::Function)
    idx1 = get_p_idx(evs,par1); idx2 = get_p_idx(evs,par2);
    grid1 = evs.parameter_grids[idx1]; grid2 = evs.parameter_grids[idx2]; 
    return [f(EvaluationVector(evs,par3,setindex!(setindex!(deepcopy(p_base),p1,idx1),p2,idx2))) for p1 in grid1, p2 in grid2]
    
end
function plot_evaluation(evs::Evaluations,par1::Symbol,par2::Symbol,par3::Symbol,p_base::Vector{Float64},f::Function;hm_min=-Inf,hm_max=Inf,legend=:right,
                        value_grid=calc_evaluations(evs,par1,par2,par3,p_base,f),kwargs...)
    return heatmap(evs.parameter_grids[get_p_idx(evs,par2)],evs.parameter_grids[get_p_idx(evs,par1)],value_grid;xguide="$(par2)",yguide="$(par1)",clim=(hm_min,hm_max),legend=legend,kwargs...)
end