### ----- ----- Preparations ----- ----- ###

import Pkg
Pkg.DEFAULT_IO[] = stdout;


### Fetch Files ###

include("Functions/models.jl");
include("Functions/simulations.jl");
include("Functions/bifurcation_diagrams.jl");
include("Functions/basic_plotting.jl");
include("Functions/data_handling.jl");
include("Functions/data_investigation.jl");
include("Functions/parameter_scan_plotting.jl");


### Other ###

gr(); 
mm = Plots.mm
default(framestyle=:box,grid=false,fmt=:png,guidefontsize=14,legendfontsize=12);


### Function ###

function monte_carlo_plot(sols;lw1=5,lw2=2,la1=0.6,la2=0.9,color1=:grey,colors2=[:pink,:lightgreen,:skyblue],sigB_idx=7,activation_lw=5,activation_la=0.8,ymax=Inf,kwargs...)
    plot() 
    (length(sols)>1) && plot!(sols[2],vars=[sigB_idx],lw=lw2,la=la2,color=colors2[1])
    (length(sols)>2) && plot!(sols[3],vars=[sigB_idx],lw=lw2,la=la2,color=colors2[2])
    (length(sols)>3) && plot!(sols[4],vars=[sigB_idx],lw=lw2,la=la2,color=colors2[3])
    plot!(sols[1];vars=[sigB_idx],lw=lw1,la=la1,color=color1,legend=:none,xguide="Time (Hours)",yguide="[σᴮ] (µM)",kwargs...)
    maxY = maximum(sol -> (ymax==Inf ? 1.05*maximum(getindex.(sol.u, sigB_idx)) : max(ymax,1.05*maximum(getindex.(sol.u, sigB_idx)))),sols.u[1:end])
    return plot_vertical_line!(0,maxY;base=-maxY/20.0,lw=activation_lw,la=activation_la,left_margin=5mm,xlimit=(sols[1].t[1],sols[1].t[end]))
end

function behavioural_transition_simulations(pProds,p_change;sT=10.0,l=200.0,model=narula_new_params_model)
    return map(Prod -> stochsim_activation(model,sT,l;p_changes=[p_change...,:pProd=>pProd],adaptive=false,dt=0.00001), pProds);
end
function behavioural_transition_plots(sols,ymaxes)
    return map(i -> plot_stochsim_activation(sols[i],ymaxes[i]), 1:length(sols))
end

# Gillespie monte-carlo simulations.
function gillespie_monte_sim(p_vals, tspan, n; saveat=0.1)
    dprob_u0 = DiscreteProblem(narula_system,[1,1,1,1,1,1,1,1,0,Int64(p_vals[23][2])],(0.0,20.0),p_vals)
    jprob_u0 = JumpProblem(narula_system, dprob_u0, Direct(),save_positions=(false,false))
    eprob_u0 = EnsembleProblem(jprob_u0,prob_func=(p,i,r)->p, safetycopy=false)
    u0s = map(sol -> sol.u[end], solve(eprob_u0,SSAStepper(); trajectories=n).u)

    dprob = DiscreteProblem(narula_system,[1,1,1,1,1,1,1,1,0,Int64(p_vals[23][2])],tspan,p_vals)
    jprob = JumpProblem(narula_system, dprob, Direct(),save_positions=(false,false))
    eprob = EnsembleProblem(jprob,prob_func=(p,i,r)->remake(p;p=deepcopy(last.(p_vals)),u0=u0s[i]),safetycopy=false)
    cb = PresetTimeCallback([0.0],integrator->(integrator.u[10]+=(integrator.p[24]-integrator.p[23])))

    solve(eprob, SSAStepper(); trajectories=n, callback=cb, saveat=saveat)
end;

### ----- ----- Analysis ----- ----- ###


### Figure 2 ###

# Pulse plots
noise_levels = [0.025,0.05,0.075,0.10]
@time simss = map(η -> monte_activation(narula_model, 5.0, 5.0, 50; p_changes=[:pInit=>0.001,:η=>η,:pStress=>0.8,:kK2=>36], saveat=0.01, adaptive=false,dt=0.00001), noise_levels)
mean_trajectory(sims) = map(i -> mean(map(sim->sim.u[i][7], sims)), 1:length(sims[1].t))
mean_trajectories = mean_trajectory.(simss)
labels = ["η=$(noise_levels[1])" "η=$(noise_levels[2])" "η=$(noise_levels[3])" "η=$(noise_levels[4])"]
mean_pulse_plot = plot(simss[1][1].t,mean_trajectories,xlimit=(-1.0,2.0),ylimit=(-0.05,3.0),lw=5,la=0.8,label=labels,xguide="Time (Hours)",yguide="[σᴮ] (µM)",size=(600,350))

ps = monte_carlo_plot.(simss,ymax=3.0)
pulse_plots = plot(ps...,layout=(4,1),xguide="",yguide="",xticks=[],yticks=[],xlimit=(-1.0,2.0),ylimit=(-0.05,3.0),size=(600,350))

pulse_plot = plot(mean_pulse_plot, pulse_plots,size=(1200,350))

# Scan plots
scanning_set = "core_parameters_scan" 
model = narula_model
p_changes_fixed = []
pStress_grid = lin_grid(0.02,2.,100)
kK2_grid = lin_grid(5,35,31) 
η_grid = lin_grid(0.0,0.15,31)
scanning_parameters = [:pStress => pStress_grid, :kK2 => kK2_grid, :η => η_grid]
@time evs_1 = Evaluations(scanning_set)

p_srp_dist = plot_evaluation(evs_1,:η,:kK2,:pStress,[0.0,0.0,0.0],max_srp,hm_max=150)
p_sp_dist = plot_evaluation(evs_1,:η,:kK2,:pStress,[0.0,0.0,0.0],max_sp,hm_max=50)
behaviour_distinctness_distribution_plots = plot(p_srp_dist,p_sp_dist,size=(1400,350),left_margin=7mm,bottom_margin=7mm)

sT = 10.0; l = 50.0; n = 4
srp_activations = monte_activation(narula_model,sT,l,n; p_changes=[:kK2=>15.0,:kB5=>3600.0,:kD5=>18.0,:kP=>180.0,:pInit=>0.05,:η=>0.04,:pStress=>0.4], saveat=0.01, adaptive=false,dt=0.00001)
sp_activations = monte_activation(narula_model,sT,l,n; p_changes=[:kK2=>9.0,:kB5=>3600.0,:kD5=>18.0,:kP=>180.0,:pInit=>0.05,:η=>0.06,:pStress=>0.225], saveat=0.01, adaptive=false,dt=0.00001)

# Behaviour examples plots.
srp_plot = monte_carlo_plot(srp_activations)
sp_plot = monte_carlo_plot(sp_activations)
behaviour_example_plots = plot(srp_plot,sp_plot,size=(1200,350),bottom_margin=5mm,left_margin=6mm)

# Gillespie simulations
ssa_srp_params = [  :kBw => 3600, :kDw => 18, :kD => 18, 
                    :kB1 => 3600, :kB2 => 3600, :kB3 => 3600, :kB4 => 1800, :kB5 => 3600, 
                    :kD1 => 18, :kD2 => 18, :kD3 => 18, :kD4 => 1800, :kD5 => 18, 
                    :kK1 => 36, :kK2 => 18, :kP => 180, :kDeg => 0.1, 
                    :v0 => 1.0, :F => 300, :K => 0.02, :λW => 4, :λV => 4.5, 
                    :pInit => 0, :pStress => 58, :η => 0.0]
ssa_srp_activations = gillespie_monte_sim(ssa_srp_params,(-10,50.0),4)
srp_gillespie_plot = monte_carlo_plot(ssa_srp_activations)

ssa_sp_params = [   :kBw => 3600, :kDw => 18, :kD => 18, 
                    :kB1 => 3600, :kB2 => 3600, :kB3 => 3600, :kB4 => 1800, :kB5 => 3600, 
                    :kD1 => 18, :kD2 => 18, :kD3 => 18, :kD4 => 1800, :kD5 => 18, 
                    :kK1 => 36, :kK2 => 18, :kP => 180, :kDeg => 0.1, 
                    :v0 => 1.6, :F => 300, :K => 0.02, :λW => 4, :λV => 4.5, 
                    :pInit => 0, :pStress => 115, :η => 0.0];
ssa_sp_activations = gillespie_monte_sim(ssa_sp_params,(-10,50.0),4)
sp_gillespie_plot = monte_carlo_plot(ssa_sp_activations)

behaviour_gillespie_example_plots = plot(srp_gillespie_plot,sp_gillespie_plot,size=(1200,350),bottom_margin=5mm,left_margin=6mm)

# Save figures.
save_figure(pulse_plot,"Figure 2";tag="pulse_")
save_figure(behaviour_distinctness_distribution_plots,"Figure 2";tag="behaviour_distinctness_distribution_")
save_figure(behaviour_example_plots,"Figure 2";tag="behaviour_example_")
save_figure(behaviour_gillespie_example_plots,"Figure 2";tag="behaviour_gillespie_example_")


### Supplementary Figure 1 ###
# Attepting to get pulsing by tuning only eta and pStress.

sT = 10.0; l = 50.0; n = 4;
@time noise_modulation_sims = [monte_activation(narula_model,sT,l,n; p_changes=[:η=>η,:pStress=>pStress], saveat=0.01, adaptive=false,dt=0.00001) for pStress in 0.3:0.3:1.5, η in 0.04:0.04:0.20]
noise_modulation_plots = monte_carlo_plot.(noise_modulation_sims,ymax=4.0;ylimit=(0.,4.0))
noise_modulation_plot = plot(noise_modulation_plots...,layout=size(noise_modulation_plots),size=(2400,1800))

save_figure(plot!(noise_modulation_plot;xticks=[],yticks=[]),"Supplementary_Figures/Supplementary_Figure_1")


### Supplementary Figure 2 ###
# Scanning bifurcation diagrams to find parameters causing oscilaltions.

stress_values = [0.05,0.2,0.8]
steady_states = map(sv -> (narula_bif_model[:pStress]=sv; get_u0_rre(narula_bif_model)[7];), stress_values)
@time for par = narula_bif_model.p_syms[1:22]
    bifs = BifurcationDiagrams(narula_bif_model,(narula_bif_model[par]/10.0,narula_bif_model[par]*10.0),par,stress_values,:pStress;var=:σB);
    serialize("../Data/Bifurcation_diagrams/bifs_$(par)_$(narula_bif_model[par]/10.0)_$(narula_bif_model[par]*10.0).jls",bifs)
end

bifs = map(par -> deserialize("../Data/Bifurcation_diagrams/bifs_$(par)_$(narula_bif_model[par]/10.0)_$(narula_bif_model[par]*10.0).jls"), narula_bif_model.p_syms[1:22]);
pStress_bif_plot = make_plot_bif(narula_bif_model,(0.1,10.0),:pStress;var=:σB,lw=5,cS=:blue,la=0.8,xaxis=:log10,ylimit=(0.,20.0));

yM1 = 0.05; yM2 = 0.10; yM3 = 2.0; yM4 = 20.0;
yMaxes = [fill(yM1,5)...,yM2,fill(yM1,4)...,yM2,yM1,yM1,yM2,yM4,yM4,fill(yM3,3)...,yM2,yM4,yM3,yM4]
bif_plots = map(i -> plot_bif_set(bifs,i,steady_states,ylimit=(0.,yMaxes[i])), 1:length(bifs));bifurcation_diagram_plots = plot(bif_plots...,pStress_bif_plot,p0,p0,layout=(5,5),size=(1800,1600),left_margin=5mm,bottom_margin=5mm)

save_figure(bifurcation_diagram_plots,"Supplementary_Figures/Supplementary_Figure_2")


### Supplementary Figure 3 ###
# Bifurcation diagrams with periodic orbits drawn out.

# This figure is handled in a separate file (under PeriodicOrbitsBifurcationDiagrams).


### Supplementary Figure 4 ###
# Automatic meassure of behaviour magnitude.

sT = 10.0; l = 50.0;
sim_srp = stochsim_activation(narula_model,sT,l;p_changes=[:kK2=>12.0,:kP=>320.0,:pInit=>0.01,:η=>0.05,:pStress=>0.165],adaptive=false,dt=0.00001)
sim_sp = stochsim_activation(narula_model,sT,l;p_changes=[:kK2=>12.0,:kP=>255.0,:pInit=>0.20,:η=>0.05,:pStress=>0.23],adaptive=false,dt=0.00001)

max_all_srp = maximum(getindex.(sim_srp.u,7))
max_poststress_srp = maximum(getindex.(sim_srp.u[findfirst(sim_srp.t .> 3):end],7))
plot_stochsim_activation(narula_model,sT,l;sol=sim_srp,title="Degree of initial pulsing = $(short_dec(max_all_srp/max_poststress_srp))")
plot!([-10.,50.],[max_all_srp, max_all_srp], label="Transient maximum (=$(short_dec(max_all_srp)))",lw=5,linestyle=:dot,la=0.8,color=:cyan)
srp_meassure_plot = plot!([-10.,50.],[max_poststress_srp, max_poststress_srp], label="Asymptotic maximum (=$(short_dec(max_poststress_srp)))",lw=5,linestyle=:dot,la=0.8,color=:magenta)

max_poststress_sp = maximum(getindex.(sim_sp.u[findfirst(sim_sp.t .> 3):end],7))
mean_poststress_sp = mean(getindex.(sim_sp.u[findfirst(sim_sp.t .> 3):end],7))
plot_stochsim_activation(narula_model,sT,l;sol=sim_sp,title="Degree of initial pulsing = $(short_dec(max_poststress_sp/mean_poststress_sp))")
plot!([-10.,50.],[max_poststress_sp, max_poststress_sp], label="Asymptotic maximum (=$(short_dec(max_poststress_sp)))",lw=5,linestyle=:dot,la=0.8,color=:cyan)
sp_meassure_plot = plot!([-10.,50.],[mean_poststress_sp, mean_poststress_sp], label="Asymptotic mean (=$(short_dec(mean_poststress_sp)))",lw=5,linestyle=:dot,la=0.8,color=:magenta)

behaviour_definitions_plots = plot(srp_meassure_plot,sp_meassure_plot,size=(1300,425),bottom_margin=5mm,left_margin=5mm)

save_figure(behaviour_definitions_plots,"Supplementary_Figures/Supplementary_Figure_4")


### Additional Code 1 ###
# Check that the transient phase is never over 5 hours

pStress_grid = lin_grid(0.02,2.,100)
kK2_grid = lin_grid(5,35,31) 
η_grid = lin_grid(0.0,0.15,31)
kP_grid = lin_grid(10,1000,100) 
pStress_grid = lin_grid(0.02,2.0,100) 
kB5_grid = [360.0,3600.0]
kD5_grid = [1.8, 18.0]

function check_transient(n)
    for i = 1:n
        pc = [:kK2 => rand(kK2_grid), :η => rand(η_grid), :kP => rand(kP_grid), :pStress => rand(pStress_grid), :kB5 => rand(kB5_grid), :kD5 => rand(kD5_grid)]
        sols = monte_activation(narula_model,5.0,10.0,1; p_changes=pc, saveat=0.01, adaptive=false,dt=0.00001)
        foreach(s -> check_sol(s,pc), sols)
    end
end
function check_sol(sol,pc)
    max1 = maximum(getindex.(sol.u[1:1000],7))
    min1 = minimum(getindex.(sol.u[1002:end],7))
    mean1 = mean(getindex.(sol.u[980:1001],7))
    mean2 = mean(getindex.(sol.u[1002:1023],7))
    mean3 = mean(getindex.(sol.u[end-20:end],7))   # Ensures that it is just not something going to a big steady state.
    # (Assumptiosn, given we have pulses < 1 hour, if there are pulses >10 hour long, tehre should also be some with length between 5 and 10 hours)
    (mean2>max(max1,mean1,mean3)) && (mean2>0.8min1) && println("Found a set: $(pc)")
end

# These should generate no messages. Those that does can be manually checked through "monte_activation".
@time check_transient(2000)
@time check_transient(2000)
@time check_transient(2000)
@time check_transient(2000)
@time check_transient(2000)



### Supplementary Figure 5 ###
# Behaviours reproduced in model without any noise in upstreams reactions.

sT = 10.0; l = 200.0; n = 4;
@time srp_activations = monte_activation(noise_modulation_model,sT,l,n; p_changes=[:kK2 => 7.0, :pFrac =>100.0, :ηFreq => 1.0, :pProd => 50, :ηAmp => 0.0, :ηCore => 0.01], saveat=0.01, adaptive=false,dt=0.00001)
@time sp_activations = monte_activation(noise_modulation_model,sT,l,n; p_changes=[:kK2 => 7.0, :pFrac =>100.0, :ηFreq => 1.0, :pProd => 25, :ηAmp => 0.0, :ηCore => 0.09], saveat=0.01, adaptive=false,dt=0.00001);

srp_plot = monte_carlo_plot(srp_activations)
sp_plot = monte_carlo_plot(sp_activations)
behaviour_definitions_plots = plot(srp_plot,sp_plot,size=(1200,550),bottom_margin=5mm,left_margin=6mm,layout=(2,1))

save_figure(behaviour_definitions_plots,"Supplementary_Figures/Supplementary_Figure_5")


### Supplementary Figure 6 ###
# Investigation of the system at the time just before a -pulse 

# Make simulation(s)
sT = 10.0; l = 750.0; n = 4;
sp_sols = monte_activation(narula_model,sT,l,n; p_changes=[:kK2=>9.0,:kB5=>3600.0,:kD5=>18.0,:kP=>180.0,:pInit=>0.05,:η=>0.055,:pStress=>0.225], saveat=0.01, adaptive=false,dt=0.00001)

# Find peaks.
traj = getindex.(sp_sols[1].u,7)
idxs_pre = findall((traj[1:end-1].<1.0) .&& (traj[2:end].>1.0))
idxs = filter(x -> (ii = findfirst(idxs_pre.==x); (idxs_pre[ii]-idxs_pre[ii-1])>200), idxs_pre[2:end-1])

# Get sections around peaks.
function extract_trajcs(sol,idx;pre=200,post=10,freq=1)
    i1 = idx-pre; 
    i2 = idx+post;
    t = sol.t[i1:freq:i2]
    w = getindex.(sol.u[i1:freq:i2],1)
    w2 = getindex.(sol.u[i1:freq:i2],2)
    w2v = getindex.(sol.u[i1:freq:i2],3)
    v = getindex.(sol.u[i1:freq:i2],4)
    w2v2 = getindex.(sol.u[i1:freq:i2],5)
    vP = getindex.(sol.u[i1:freq:i2],6)
    σB = getindex.(sol.u[i1:freq:i2],7)
    w2σB = getindex.(sol.u[i1:freq:i2],7)
    vPp = getindex.(sol.u[i1:freq:i2],8)
    phos = getindex.(sol.u[i1:freq:i2],10);
    return (σB.+w2σB,w.+2*w2.+2*w2v.+2*w2v2.+2*w2σB,w2v.+v.+2*w2v2,t)
end
outs = map(idx -> extract_trajcs(sp_sols[1],idx), idxs)

# Plot trajectories (doublechek trajectories, sometimes the peaks is just after a rpevious one, so levels are going down in a different way, skip these).
function plot_traj(out)
    σB = out[1]; traj = out[2]; t = out[4];
    traj_mod = (traj .- minimum(traj)) ./ maximum(traj .- minimum(traj))
    traj_mod = traj ./ maximum(traj)
    plot(t,σB ./ 3.0,xguide="time",yguide="",label="sigB",lw=5)
    p1 = plot!(t,traj ./ 20.0,xguide="time",yguide="",label="RsbW_tot",legend=:topleft,lw=5,xlimit=(t[1],t[end]),ylimit=(0.0,1.1))
    plot(traj,σB,yguide="sigB",xguide="RsbW_tot",label="",lw=2,la=0.6,xlimit=(9.0,12.0),ylimit=(0.0,1.5))
    p2 = scatter!((traj[1],σB[1]),color=1,ms=5)
    plot(p1,p2,size=(1000,550),bottom_margin=5mm,left_margin=5mm,legend=:none,xguide="",yguide="",xticks=[],yticks=[],layout=@layout[a{0.5h};b])
end;
p1 = plot_traj(outs[1])
p2 = plot_traj(outs[2])
p3 = plot_traj(outs[3])
p4 = plot_traj(outs[4])
pusle_activation_plots = plot(p1,p2,p3,p4,layout=(1,4),size=(2000,1000))

save_figure(pusle_activation_plots,"Supplementary_Figures/Supplementary_Figure_6") 


### Supplementary Figure 7 ###
### This figure was not included in the paper, hence following figures may be miss-numbered.
# Reproducing the two behaviours in a model where SigB faces competiotn for RNAPolymerase from another sigam factor (SigA).

sT = 10.0; l = 200.0; n = 4;
@time competition_srp_sim = monte_activation(narula_competition_model,sT,l,n; p_changes=[:v0=>0.05, :kK2=>15.0,:kB5=>3600.0,:kD5=>18.0,:kP=>180.0,:pInit=>0.05,:η=>0.04,:pStress=>0.4], saveat=0.01, adaptive=false,dt=0.00001);
@time competition_sp_sim = monte_activation(narula_competition_model,sT,l,n;  p_changes=[:v0=>0.05, :kK2=>7.0,:kB5=>3600.0,:kD5=>18.0,:kP=>180.0,:pInit=>0.05,:η=>0.06,:pStress=>0.16], saveat=0.01, adaptive=false,dt=0.00001);

srp_plot = monte_carlo_plot(competition_srp_sim;activation_la=0.5,activation_lw=3,ymax=2.5)
sp_plot = monte_carlo_plot(competition_sp_sim;activation_la=0.5,activation_lw=3,ymax=4.5)
competition_both_behaviour_plot = plot(srp_plot,sp_plot,size=(1200,550),bottom_margin=5mm,left_margin=6mm,layout=(2,1))

save_figure(competition_both_behaviour_plot,"Supplementary_Figures/Supplementary_Figure_7")

### Supplementary Figure 8 ###
# Generate behaviour robustly.

eV = EvaluationVector(evs_1,:pStress,[0.0,7.0,0.025])
make_behaviour_distinctness_area_plot(eV,srp_degree,sp_degree;display_data=true)
plot!(xlimit=(0.02,0.8))
save_figure(behaviour_distinctness_definition_plots,"Supplementary_Figures/Supplementary_Figure_8")


### Supplementary Figure 9 ###
# Scanning parameter space to find optimal kK2 and eta.

scanning_set = "core_parameters_scan"
model = narula_model
p_changes_fixed = []
pStress_grid = lin_grid(0.02,2.,100)
kK2_grid = lin_grid(5,35,31) 
η_grid = lin_grid(0.0,0.15,31)
scanning_parameters = [:pStress => pStress_grid, :kK2 => kK2_grid, :η => η_grid];
@time evs_1 = Evaluations(scanning_set);

fieldnames(typeof(evs_1.evaluations.vals[1]))
evs_1.evaluations.vals[1]


value_grid = calc_evaluations(evs_1,:η,:kK2,:pStress,[0.0,0.0,0.0],srp_n_sp_degree)
plot_evaluation(evs_1,:η,:kK2,:pStress,[0.0,0.0,0.0],srp_n_sp_degree;value_grid=value_grid)
aM = argmax(value_grid);
determening_core_parameters_plots = scatter!((kK2_grid[aM[2]],η_grid[aM[1]]),label="",color=:darkslategray1,markersize=5)

eV = EvaluationVector(evs_1,:pStress,[0.0,7.0,0.025])
argmax(sp_degree.(eV.evaluations))
println(lin_grid(0.02,2.,100)[14])

save_figure(determening_core_parameters_plots,"Supplementary_Figures/Supplementary_Figure_9")


### Supplementary Figure 10 ###
# Generate behaviour optimally by tuning upstream parameters of kK2 and eta.

sT = 10.0; l = 200.0; n = 4;
@time srp_activations = monte_activation(narula_model,sT,l,n; p_changes=[:kK2=>7.0,:kB5=>3600.0,:kD5=>18.0,:kP=>180.0,:pInit=>0.05,:η=>0.025,:pStress=>0.24], saveat=0.01, adaptive=false,dt=0.00001)
@time sp_activations = monte_activation(narula_model,sT,l,n; p_changes=[:kK2=>7.0,:kB5=>3600.0,:kD5=>18.0,:kP=>180.0,:pInit=>0.05,:η=>0.025,:pStress=>0.28], saveat=0.01, adaptive=false,dt=0.00001)

srp_plot = monte_carlo_plot(srp_activations)
sp_plot = monte_carlo_plot(sp_activations)
plot_both_behaviours_optimal = plot(srp_plot,sp_plot,size=(1200,550),bottom_margin=5mm,left_margin=6mm,layout=(2,1))

save_figure(plot_both_behaviours_optimal,"Supplementary_Figures/Supplementary_Figure_10")


### Supplementary Figure 11 ###
# Behaviour scan in kP, kB5, kD5, and pStress space.

scanning_set = "kP_pStress_scan"
model = narula_model
p_changes_fixed = [:kK2 => 7.0, :η => 0.025]
kP_grid = lin_grid(10,1000,100) 
pStress_grid = lin_grid(0.02,2.0,100) 
kB5_grid = [360.0,3600.0]
kD5_grid = [1.8, 18.0]
scanning_parameters = [:kP => kP_grid, :pStress => pStress_grid, :kB5 => kB5_grid, :kD5 => kD5_grid];
evs_2 = Evaluations(scanning_set)

srp_plots = [plot_evaluation(evs_2,:kP,:pStress,[0.,0.,kB5,kD5],srp_degree,title="kB5=$(kB5), kD5=$(kD5)",hm_max=60.0) for kB5 in kB5_grid, kD5 in kD5_grid]
srp_plot = plot(srp_plots...,layout=(2,2),size=(1200,700),bottom_margin=5mm,left_margin_margin=5mm)
sp_plots = [plot_evaluation(evs_2,:kP,:pStress,[0.,0.,kB5,kD5],sp_degree,title="kB5=$(kB5), kD5=$(kD5)",hm_max=60) for kB5 in kB5_grid, kD5 in kD5_grid]
sp_plot = plot(sp_plots...,layout=(2,2),size=(1200,700),bottom_margin=5mm,left_margin_margin=5mm)
kP_pStress_untransformed_additional_plots = plot(srp_plot,sp_plot,size=(3000,800),bottom_margin=15mm)

save_figure(kP_pStress_untransformed_additional_plots,"Supplementary_Figures/Supplementary_Figure_11")


### Figure 3 ###

scanning_set = "kP_pStress_scan"
model = narula_model
p_changes_fixed = [:kK2 => 7.0, :η => 0.025]
kP_grid = lin_grid(10,1000,100) 
pStress_grid = lin_grid(0.02,2.0,100) 
kB5_grid = [360.0,3600.0]
kD5_grid = [1.8, 18.0]
scanning_parameters = [:kP => kP_grid, :pStress => pStress_grid, :kB5 => kB5_grid, :kD5 => kD5_grid]
evs_2 = Evaluations(scanning_set)

eM = EvaluationMatrix(evs_2,:kP,:pStress,[0.,0.,3600.0,18.0]); xs = 0.02:0.01:2.0;
(g1,g2,g3,g4) = [RGB{Float64}(0.,1.,0.8),RGB{Float64}(0.1,0.8,0.4),RGB{Float64}(0.2,0.6,0.2),RGB{Float64}(0.3,0.4,0.0)]

plot_evaluation(eM,srp_degree,title="Degree of Initial Pulsing")
plot!(xs,25 ./xs,color=g1,lw=3,la=0.90,linestyle=:dash,label="pStress*kP = 25")
plot!(xs,50 ./xs,color=g2,lw=3,la=0.90,linestyle=:dash,label="pStress*kP = 50")
plot!(xs,75 ./xs,color=g3,lw=3,la=0.90,linestyle=:dash,label="pStress*kP = 75")
plot!(xs,100 ./xs,color=g4,lw=3,la=0.90,linestyle=:dash,label="pStress*kP = 100")
p1 = plot!(xlimit=(eM.grid2[1],eM.grid2[end]),ylimit=(eM.grid1[1],eM.grid1[end]),legendfontsize=12)

plot_evaluation(eM,sp_degree,title="Degree of Stochastic Pulsing")
plot!(xs,25 ./xs,color=g1,lw=3,la=0.90,linestyle=:dash,label="pStress*kP = 25")
plot!(xs,50 ./xs,color=g2,lw=3,la=0.90,linestyle=:dash,label="pStress*kP = 50")
plot!(xs,75 ./xs,color=g3,lw=3,la=0.90,linestyle=:dash,label="pStress*kP = 75")
plot!(xs,100 ./xs,color=g4,lw=3,la=0.90,linestyle=:dash,label="pStress*kP = 100")
p2 = plot!(xlimit=(eM.grid2[1],eM.grid2[end]),ylimit=(eM.grid1[1],eM.grid1[end]),legendfontsize=12)

kP_pStress_untransformed_plot = plot(p1,p2,size=(1400,425),bottom_margin=7mm,left_margin=7mm)

scanning_set = "kP_pTot_transformed_scan"
model = narula_new_params_model
p_changes_fixed = [:kK2 => 7.0, :η => 0.025]
pFrac_grid = log_grid(10,10000,100) 
pProd_grid = lin_grid(20,200.0,100) 
kB5_grid = [360.0,3600.0]
kD5_grid = [1.8, 18.0]
scanning_parameters = [:pFrac => pFrac_grid, :pProd => pProd_grid, :kB5 => kB5_grid, :kD5 => kD5_grid];
evs_3 = Evaluations(scanning_set);

eM = EvaluationMatrix(evs_3,:pFrac,:pProd,[0.,0.,3600.0,18.0]);
p1 = plot_evaluation(eM,srp_degree,title="Degree of Initial Pulsing",yaxis=:log)
p2 = plot_evaluation(eM,sp_degree,title="Degree of Stochastic Pulsing",yaxis=:log)
(g1,g2,g3,g4) = [RGB{Float64}(0.,1.,0.8),RGB{Float64}(0.1,0.8,0.4),RGB{Float64}(0.2,0.6,0.2),RGB{Float64}(0.3,0.4,0.0)]

plot(p1); 
plot!([25.0,25.0],[0.1,10000.],color=g1,lw=3,la=0.90,linestyle=:dash,label="pStress*kP = 25")
plot!([50.0,50.0],[0.1,10000.],color=g2,lw=3,la=0.90,linestyle=:dash,label="pStress*kP = 50")
plot!([75.0,75.0],[0.1,10000.],color=g3,lw=3,la=0.90,linestyle=:dash,label="pStress*kP = 75")
p3 = plot!(xlimit=(eM.grid2[1],eM.grid2[end]),ylimit=(eM.grid1[1],eM.grid1[end]),legendfontsize=12)

plot(p2); 
plot!([50.0,50.0],[0.1,10000.],color=g2,lw=3,la=0.90,linestyle=:dash,label="pStress*kP = 50")
plot!([75.0,75.0],[0.1,10000.],color=g3,lw=3,la=0.90,linestyle=:dash,label="pStress*kP = 75")
plot!([100.0,100.0],[0.1,10000.],color=g4,lw=3,la=0.90,linestyle=:dash,label="pStress*kP = 100")
p4 = plot!(xlimit=(eM.grid2[1],eM.grid2[end]),ylimit=(eM.grid1[1],eM.grid1[end]),legendfontsize=12)

plot_evaluation(evs_3,:pProd,[100.,0.,3600.0,18.0],srp_degree,lw=7,la=0.5,label="Initial Pulsing (PFrac = 100)",color=4)
plot_evaluation!(evs_3,:pProd,[10.,0.,3600.0,18.0],srp_degree,lw=2,la=1,label="Initial Pulsing (PFrac = 10)",color=:fuchsia)
p5 = plot_evaluation!(evs_3,:pProd,[1000.,0.,3600.0,18.0],srp_degree,lw=2,la=1,label="Initial Pulsing (PFrac = 1000)",color=:purple4)
plot_evaluation(evs_3,:pProd,[100.,0.,3600.0,18.0],sp_degree,lw=7,la=0.5,color=6,label="Stochastic Pulsing (PFrac = 100)",yguide="Degree of behaviour",legendfontsize=11,guidefontsize=14,tickfontsize=10,right_margin=2mm)
plot_evaluation!(evs_3,:pProd,[10.,0.,3600.0,18.0],sp_degree,lw=2,la=1,label="Stochastic (PFrac = 10)",color=:cyan)
p6 = plot_evaluation!(evs_3,:pProd,[1000.,0.,3600.0,18.0],sp_degree,lw=2,la=1,label="Stochastic (PFrac = 1000)",color=:teal)
plot(p1,p2,size=(1100,350))

pProd_pFrac_transformed_plot = plot(p1,p2,size=(1400,425),bottom_margin=7mm,left_margin=7mm)

scanning_set = "phosphatase_parameters_scan"
model = narula_new_params_model
p_changes_fixed = [:kK2 => 7.0, :η => 0.025]
pProd_grid = lin_grid(20,150,16)
pFrac_grid = lin_grid(10,1000,16)
kB5_grid = lin_grid(200,5000,16) 
kD5_grid = lin_grid(1,250,16)
scanning_parameters = [:pProd => pProd_grid, :pFrac => pFrac_grid, :kB5 => kB5_grid, :kD5 => kD5_grid]
evs_4 = Evaluations(scanning_set)

change_meassure(array) = sum((length(array)-1) * (diff(array) .^2));
function all_array(super_array,idx)
    idxs = Vector{Any}(map(s -> 1:s, [size(super_array)...]))
    idxs[idx] = [idxs[idx]]
    vec([super_array[i,j,k,l] for i in idxs[1], j in idxs[2], k in idxs[3], l in idxs[4]])
end

evs_grid = [evs_4.evaluations[[pFrac,pProd,kB5,kD5]] for pFrac in evs_4.parameter_grids[1], pProd in evs_4.parameter_grids[2], kB5 in evs_4.parameter_grids[3], kD5 in evs_4.parameter_grids[4]];
srp_grid = srp_degree.(evs_grid);
sp_grid = sp_degree.(evs_grid);
sorted_arrays_srp = map(i ->  sort(all_array(srp_grid,i);by=a->change_meassure(a)), 1:4)
sorted_arrays_sp = map(i ->  sort(all_array(sp_grid,i);by=a->change_meassure(a)), 1:4)

parameter_importance_plot = plot(plot(map(ars -> change_meassure.(ars), sorted_arrays_srp)),plot(map(ars -> change_meassure.(ars), sorted_arrays_sp)),lw=8,la=0.7,label=["pProd" "pFrac" "kB5" "kD5"],xlimit=(0.,4000.),size=(1100,300),legend=:topleft)

save_figure(kP_pStress_untransformed_plot,"Figure 3";tag="untransformed_")
save_figure(pProd_pFrac_transformed_plot,"Figure 3";tag="transformed_")
save_figure(parameter_importance_plot,"Figure 3";tag="parameters_")


### Supplementary Figure 12-17 ###
# Additional plots in transformed space

println(pProd_grid[1:3:16])
println(pFrac_grid[1:3:16])
println(kB5_grid[1:3:16])
println(kD5_grid[1:3:16])

supp_fig_pProd_pFrac_srp = plot_evaluation_grids(evs_4,srp_degree,:pFrac,:pProd,[0.0,0.0,0.0,0.0],:kB5,kB5_grid[1:3:16],:kD5,kD5_grid[1:3:16],hm_min=0.0,hm_max=65.0,size=(5200,1800))
supp_fig_pProd_pFrac_sp = plot_evaluation_grids(evs_4,sp_degree,:pFrac,:pProd,[0.0,0.0,0.0,0.0],:kB5,kB5_grid[1:3:16],:kD5,kD5_grid[1:3:16],hm_min=0.0,hm_max=40.0,size=(5200,1800))

supp_fig_kB5_kD5_srp = plot_evaluation_grids(evs_4,srp_degree,:kB5,:kD5,[0.0,0.0,0.0,0.0],:pProd,pProd_grid[1:3:16],:pFrac,pFrac_grid[1:3:16],hm_min=0.0,hm_max=65.0,size=(5200,1800))
supp_fig_kB5_kD5_sp = plot_evaluation_grids(evs_4,sp_degree,:kB5,:kD5,[0.0,0.0,0.0,0.0],:pProd,pProd_grid[1:3:16],:pFrac,pFrac_grid[1:3:16],hm_min=0.0,hm_max=400.0,size=(5200,1800))

supp_fig_pProd_kB5_srp = plot_evaluation_grids(evs_4,srp_degree,:kB5,:pProd,[0.0,0.0,0.0,0.0],:pFrac,pFrac_grid[1:3:16],:kD5,kD5_grid[1:3:16],hm_min=0.0,hm_max=50.0,size=(5200,1800))
supp_fig_pProd_kB5_sp = plot_evaluation_grids(evs_4,sp_degree,:kB5,:pProd,[0.0,0.0,0.0,0.0],:pFrac,pFrac_grid[1:3:16],:kD5,kD5_grid[1:3:16],hm_min=0.0,hm_max=50.0,size=(5200,1800))

save_figure(supp_fig_pProd_pFrac_srp,"Supplementary_Figures/Supplementary_Figure_12")
save_figure(supp_fig_pProd_pFrac_sp,"Supplementary_Figures/Supplementary_Figure_13")
save_figure(supp_fig_kB5_kD5_srp,"Supplementary_Figures/Supplementary_Figure_14")
save_figure(supp_fig_kB5_kD5_sp,"Supplementary_Figures/Supplementary_Figure_15")
save_figure(supp_fig_pProd_kB5_srp,"Supplementary_Figures/Supplementary_Figure_16")
save_figure(supp_fig_pProd_kB5_sp,"Supplementary_Figures/Supplementary_Figure_17")


### Figure 4 ###

scanning_set = "pProd_variation_parameters_scan"
model = narula_new_params_model
p_changes_fixed = [:kK2 => 7.0, :η => 0.025, :pFrac =>100.0]
scanning_parameters = [:pProd => pProd_grid];
evs_5 = Evaluations(scanning_set);

eV = EvaluationVector(evs_5,:pProd,[0.0])
plot([eV.grid...,255.0],[srp_degree.(eV.evaluations)...,1.0],lw=6,color=4,la=0.9,label="Initial Pulsing",xlimit=(20.0,255.0))
plot!([eV.grid...,255.0],[sp_degree.(eV.evaluations)...,1.0],lw=5,color=6,la=0.9,label="Stochastic Pulsing",yguide="Degree of behaviour",legendfontsize=11,guidefontsize=14,tickfontsize=10,right_margin=2mm)

pProd_samples = [30.0,40.0,45.0,51.0,54.0,60.0,75.0,100.0,125.0,150.0,200.0,250.0]
foreach(pPrd -> plot!([pPrd,pPrd],[0.,yMax],lw=3,color=:black,label="",la=0.6), pProd_samples)
p1 = plot!([],[],lw=5,color=:black,label="Sample pProd",la=0.3,ylimit=(0.,yMax),size=(800,300))

pStress_bif_plot = make_plot_bif(narula_new_params_bif_model,(20.0,500.0),:pProd;var=:σB,lw=5,cS=:blue,la=0.8,lsU=:solid)

foreach(pPrd -> plot!([pPrd,pPrd],[-1.,20.0],lw=3,color=:black,label="",la=0.6), pProd_samples)
p2 = plot!([],[],lw=5,color=:black,label="Sample pProd",ylimit=(0.0,18.0),la=0.3,size=(1600,300))

pProd_selections_plot = plot(p1,p2,size=(2800,300),layout=@layout [a{0.33w} b]);

ymaxes = [fill(4.0,5)...,fill(9.0,3)...,fill(18.0,4)...]
pProd_grid = [30.0, 40.0, 45.0, 51.0, 54.0, 60.0, 75.0, 100.0, 125.0, 150.0, 200.0, 250.0]
@time pProd_variation_activation_plots = map((pProd,i) -> plot_stochsim_activation(narula_new_params_model,10.0,200.0;p_changes=[:kK2 => 7.0, :η => 0.025,:pFrac=>100.0,:pProd=>pProd],adaptive=false,dt=0.00001,ymax=ymaxes[i]), pProd_grid, 1:length(pProd_grid))
pProd_variation_activation_plot = plot(pProd_variation_activation_plots...,layout=(3,4),size=(2800,900),bottom_margin=9mm,left_margin=4mm)

behavioural_change_with_input_plot = plot(pProd_selections_plot,pProd_variation_activation_plot,size=(2800,1200),layout=@layout [a{0.3h}; b])

save_figure(behavioural_change_with_input_plot,"Figure 4")
save_figure(behavioural_change_with_input_plot,"Supplementary_Figures/Supplementary_Figure_18")


### Supplementary Figure 18 ###

# Basically a repeat of Figure 4, so is handled by that section.


### Supplementary Figure 19 ###
# Reproducing the transition of various model behaviours using Gillespie simulations as the stres magnitude is modualted.

function make_input_sim(pStress)
    params = [  :kBw => 3600, :kDw => 18, :kD => 18, 
                :kB1 => 3600, :kB2 => 3600, :kB3 => 3600, :kB4 => 1800, :kB5 => 3600, 
                :kD1 => 18, :kD2 => 18, :kD3 => 18, :kD4 => 1800, :kD5 => 18, 
                :kK1 => 36, :kK2 => 18, :kP => 180, :kDeg => 0.1, 
                :v0 => 1.6, :F => 300, :K => 0.02, :λW => 4, :λV => 4.5, 
                :pInit => 0, :pStress => pStress, :η => 0.0];

    gillespie_monte_sim(params,(-10,200.0),4);
end

stress_values = [80.0, 95.0, 100.0, 110.0, 120.0, 180.0, 250.0, 400.0, 550.0, 700.0, 800.0, 1000.0]
@time stress_sims = map(sv -> make_input_sim(sv), stress_values)
ymaxes = [300.0, 300.0, 300.0, 300.0, 800, 800, 800, 800, 2400, 2400, 2400, 2400]
stress_plots = map((sols, ymax) -> monte_carlo_plot(sols; ymax=ymax, activation_lw=3,activation_la=0.6), stress_sims, ymaxes)
gillespie_stress_modulation_plot = plot(stress_plots...,layout=(3,4),size=(2800,900),bottom_margin=9mm,left_margin=4mm,xguide="",yguide="",xticks=[],yticks=[])

save_figure(gillespie_stress_modulation_plot,"Supplementary_Figures/Supplementary_Figure_19")


### Supplementary Figure 20 ###
# Show how eta_amp and eta_freq affects the input

@time noise_modulation_sims = [stochsim_activation(noise_modulation_model,2.0,100.0;p_changes=[:pProd=> 100, :ηFreq=>ηFreq,:ηAmp=>ηAmp],adaptive=false,dt=0.00025,saveat=0.1,t_ss=100.0) for ηFreq in [0.1,1.0,10.0], ηAmp in [0.01,0.05,0.1]]
noise_modulation_plots = map(sim -> plot(sim.t,map(v -> v[9]+v[10], sim.u),lw=3,label="",color=2,la=0.7,xlimit=(-2.,25.0),ylimit=(0.0,2.0)), noise_modulation_sims)
noise_modulation_plot = plot(noise_modulation_plots...,layout=(3,3),size=(1500,900),xguide="Time (Hours)",yguide="Active Phopshatase (µM)",left_margin=7mm,bottom_margin=2mm)

save_figure(noise_modulation_plot,"Supplementary_Figures/Supplementary_Figure_20")

### Figure 5 ###

scanning_set = "phosphatase_noise_parameters_scan"
model = noise_modulation_model
p_changes_fixed = [:kK2 => 7.0, :ηCore => 0.025, :pFrac =>100.0]
pProd_grid = lin_grid(20,200.0,100)
ηAmp_grid = lin_grid(0.0,0.30,31)
ηFreq_grid = log_grid(0.1,100.0,31) ;
scanning_parameters = [:pProd => pProd_grid, :ηAmp => ηAmp_grid, :ηFreq=> ηFreq_grid];
@time evs_6 = Evaluations(scanning_set);

p1 = plot_evaluations(evs_6,:ηAmp,:pProd,[0.0,0.0,ηFreq_grid[5]],srp_degree,sp_degree;hm_max=50.0,bottom_margin=7mm,size=(1000,300))
p2 = plot_evaluations(evs_6,:ηAmp,:pProd,[0.0,0.0,ηFreq_grid[10]],srp_degree,sp_degree;hm_max=50.0,bottom_margin=7mm,size=(1000,300))
p3 = plot_evaluations(evs_6,:ηAmp,:pProd,[0.0,0.0,ηFreq_grid[15]],srp_degree,sp_degree;hm_max=50.0,bottom_margin=7mm,size=(1000,300))
p4 = plot_evaluations(evs_6,:ηAmp,:pProd,[0.0,0.0,ηFreq_grid[20]],srp_degree,sp_degree;hm_max=50.0,bottom_margin=7mm,size=(1000,300))
phos_noise_params_pProd_nAmp = plot(p1,p2,p3,p4,layout=(4,1),size=(1000,1200))

p1 = plot_evaluations(evs_6,:ηFreq,:pProd,[0.0,ηAmp_grid[5],0.0],srp_degree,sp_degree;hm_max=50.0,bottom_margin=7mm,size=(1000,300),yaxis=:log10)
p2 = plot_evaluations(evs_6,:ηFreq,:pProd,[0.0,ηAmp_grid[10],0.0],srp_degree,sp_degree;hm_max=50.0,bottom_margin=7mm,size=(1000,300),yaxis=:log10)
p3 = plot_evaluations(evs_6,:ηFreq,:pProd,[0.0,ηAmp_grid[15],0.0],srp_degree,sp_degree;hm_max=50.0,bottom_margin=7mm,size=(1000,300),yaxis=:log10)
p4 = plot_evaluations(evs_6,:ηFreq,:pProd,[0.0,ηAmp_grid[20],0.0],srp_degree,sp_degree;hm_max=50.0,bottom_margin=7mm,size=(100,300),yaxis=:log10)
phos_noise_params_pProd_nFreq = plot(p1,p2,p3,p4,layout=(4,1),size=(1000,1200))

save_figure(phos_noise_params_pProd_nAmp,"Figure 5/Determening_Phosphatase_Noise_Parameters_pProd_nAmp")
save_figure(phos_noise_params_pProd_nFreq,"Figure 5/Determening_Phosphatase_Noise_Parameters_pProd_nFreq")

srp_vals = [srp_distinctness(EvaluationVector(evs_6,:pProd,[0.0,ηAmp,ηFreq])) for ηAmp in evs_6.parameter_grids[2], ηFreq in evs_6.parameter_grids[3]];
aM_srp = argmax(srp_vals);
sp_vals = [sp_distinctness(EvaluationVector(evs_6,:pProd,[0.0,ηAmp,ηFreq])) for ηAmp in evs_6.parameter_grids[2], ηFreq in evs_6.parameter_grids[3]];
aM_sp = argmax(sp_vals);

plot_evaluation(evs_6,:ηAmp,:ηFreq,:pProd,[0.0,0.0,0.0],srp_distinctness,xaxis=:log10)

plot_evaluation(evs_6,:ηAmp,:ηFreq,:pProd,[0.0,0.0,0.0],srp_distinctness,xaxis=:log10,xlimit=(0.1,110.0))
p_srp_dist = scatter!((evs_6.parameter_grids[3][31],evs_6.parameter_grids[2][2]),xlimit=(0.1,110.0),ylimit=(0.0,0.3),label="",color=:darkslategray1,markersize=5)
plot_evaluation(evs_6,:ηAmp,:ηFreq,:pProd,[0.0,0.0,0.0],sp_distinctness,xaxis=:log10,xlimit=(0.1,110.0))
p_sp_dist = scatter!((evs_6.parameter_grids[3][5],evs_6.parameter_grids[2][19]),xlimit=(0.1,110.0),ylimit=(0.0,0.3),label="",color=:darkslategray1,markersize=5)
phosphatase_noise_parameters_distribution_plots = plot(p_srp_dist,p_sp_dist,size=(1400,350),left_margin=7mm,bottom_margin=9mm)

pProd_samples_srp = [40.,43.0,46.0,49.0,52.0,55.0,58.0];
plot([srp_v.grid...,255.0],[srp_degree.(srp_v.evaluations)...,1.0],lw=6,color=4,la=0.9,label="Initial Pulsing",xlimit=(20.0,150.0))
plot!([srp_v.grid...,255.0],[sp_degree.(srp_v.evaluations)...,1.0],lw=5,color=6,la=0.9,label="Stochastic Pulsing",yguide="Degree of behaviour",legendfontsize=11,guidefontsize=14,tickfontsize=10,right_margin=2mm)

foreach(pPrd -> plot!([pPrd,pPrd],[0.,70.0],lw=3,color=:black,label="",la=0.6), pProd_samples_srp)
srp_degree_with_pProd = plot!([],[],lw=5,color=:black,label="Sample pProd",la=0.3,ylimit=(0.,70),xlimit=(35.0,65.0),size=(500,300))

pProd_samples_sp = [22.0,25.0,28.0,31.0,34.0,37.0,40.0];
plot([sp_v.grid...,255.0],[srp_degree.(sp_v.evaluations)...,1.0],lw=6,color=4,la=0.9,label="Initial Pulsing",xlimit=(20.0,150.0))
plot!([sp_v.grid...,255.0],[sp_degree.(sp_v.evaluations)...,1.0],lw=5,color=6,la=0.9,label="Stochastic Pulsing",yguide="Degree of behaviour",legendfontsize=11,guidefontsize=14,tickfontsize=10,right_margin=2mm)

foreach(pPrd -> plot!([pPrd,pPrd],[0.,70.0],lw=3,color=:black,label="",la=0.6), pProd_samples_sp)
sp_degree_with_pProd = plot!([],[],lw=5,color=:black,label="Sample pProd",la=0.3,ylimit=(0.,70.0),xlimit=(20.0,50.0),size=(500,300))

function behavioural_transition_simulations(pProds,p_change;sT=10.0,l=200.0,model=narula_new_params_model)
    return map(pProd -> stochsim_activation(model,sT,l;p_changes=[p_change...,:pProd=>pProd],adaptive=false,dt=0.00002), pProds);
end;

function behavioural_transition_plots(sols,ymaxes;sT=10.0,l=200.0,model=narula_new_params_model)
    return map(i -> plot_stochsim_activation(model,sT,l;sol=sols[i],ymax=ymaxes[i]), 1:length(sols))
end;

@time srp_trans_sols = behavioural_transition_simulations(pProd_samples_srp,[:kK2 => 7.0, :ηCore => 0.025, :pFrac =>100.0, :ηAmp => evs_6.parameter_grids[2][2], :ηFreq => evs_6.parameter_grids[3][31]]; model=noise_modulation_model);

srp_trans_plots = behavioural_transition_plots(srp_trans_sols,fill(4.0,7))
srp_trans_plot = plot(srp_trans_plots...,layout=(1,7),size=(2200,300),ylimit=(0.0,4.0))

@time sp_trans_sols = behavioural_transition_simulations(pProd_samples_sp,[:kK2 => 7.0, :ηCore => 0.025, :pFrac =>100.0, :ηAmp => evs_6.parameter_grids[2][19], :ηFreq => evs_6.parameter_grids[3][5]]; model=noise_modulation_model);

sp_trans_plots = behavioural_transition_plots(sp_trans_sols,fill(4.0,7))
sp_trans_plot = plot(sp_trans_plots...,layout=(1,7),size=(2200,300),ylimit=(0.0,4.0))

save_figure(phosphatase_noise_parameters_distribution_plots,"Figure 5";tag="heatmpas_")
save_figure(srp_degree_with_pProd,"Figure 5";tag="srp_vector_")
save_figure(sp_degree_with_pProd,"Figure 5";tag="sp_vector_")
save_figure(srp_trans_plot,"Figure 5";tag="srp_sims_")
save_figure(sp_trans_plot,"Figure 5";tag="sp_sims_")


### Figure 6 ###

scanning_set = "core_noise_v_phosphatase_noise_scan"
model = noise_modulation_model
p_changes_fixed = [:kK2 => 7.0, :pFrac =>100.0, :ηFreq => 1.0]
pProd_grid = lin_grid(20,200.0,100)
ηCore_grid = lin_grid(0.0,0.15,31)
ηAmp_grid = lin_grid(0.0,0.30,31)
scanning_parameters = [:pProd => pProd_grid, :ηCore => ηCore_grid, :ηAmp => ηAmp_grid]
@time evs_7 = Evaluations(scanning_set);

p_srp_dist = plot_evaluation(evs_7,:ηCore,:ηAmp,:pProd,[0.0,0.0,0.0],srp_distinctness,hm_min=0)
p_sp_dist = plot_evaluation(evs_7,:ηCore,:ηAmp,:pProd,[0.0,0.0,0.0],sp_distinctness,hm_min=0)
p1 = plot(p_srp_dist,p_sp_dist,size=(1400,350),left_margin=7mm,bottom_margin=7mm)

core_v_upstream_noise_plot = plot(p1,size=(2800,700))
save_figure(core_v_upstream_noise_plot,"Figure 6")