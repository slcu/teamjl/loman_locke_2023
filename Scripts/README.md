### Scripts

This folder contains a jupyter notebook, "make_figures.ipynb", used for generating the figures in the paper. Once generated, the figures are stored in the "Figures" folder. All figures are subject to some amount of post-processing. Many functions, and other snippets of code used for this chapter, are stored in the "Functions" folder. 
