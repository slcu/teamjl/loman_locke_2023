### Initiation ###
iter_this = (length(ARGS)<1) ? 1 : parse(Int64,ARGS[1])
iter_tot = (length(ARGS)<2) ? 1 : parse(Int64,ARGS[2])
scanning_set = (length(ARGS)<3) ? "none" : ARGS[3]

### Fetch Required Files ###
include("Functions/models.jl");
include("Functions/scanning_functions.jl");

### Makes the designated scan ###

# Scans the core parameters kK2 and η (and pStress), finding their optimal value.
if scanning_set == "core_parameters_scan"
    model = igoshin_model
    p_changes_fixed = []
    pStress_grid = lin_grid(0.02,2.,100)
    kK2_grid = lin_grid(5,35,31)
    η_grid = lin_grid(0.0,0.15,31)
    scanning_parameters = [:pStress => pStress_grid, :kK2 => kK2_grid, :η => η_grid];

    #scan_parameters(scanning_set,iter_this,iter_tot,model,p_changes_fixed,scanning_parameters;n=150)
    rescan_parameters(9999,scanning_set,iter_this,iter_tot,model,p_changes_fixed,scanning_parameters;n=150)
end

# Scans kP and pTot, showing that their product is important.
if scanning_set == "kP_pStress_scan"
    model = igoshin_model
    p_changes_fixed = [:kK2 => 7.0, :η => 0.025]
    kP_grid = lin_grid(10,1000,100)
    pStress_grid = lin_grid(0.02,2.0,100)
    kB5_grid = [360.0,3600.0]
    kD5_grid = [1.8, 18.0]
    scanning_parameters = [:kP => kP_grid, :pStress => pStress_grid, :kB5 => kB5_grid, :kD5 => kD5_grid]

    scan_parameters(scanning_set,iter_this,iter_tot,model,p_changes_fixed,scanning_parameters)
end

# Demonstrates the point of the last scan, by performing a scan for pProd and pFrac.
if scanning_set == "kP_pTot_transformed_scan"
    model = igoshin_new_params_model
    p_changes_fixed = [:kK2 => 7.0, :η => 0.025]
    pFrac_grid = log_grid(10,10000,100)
    pProd_grid = lin_grid(20,200.0,100)
    kB5_grid = [360.0,3600.0]
    kD5_grid = [1.8, 18.0]
    scanning_parameters = [:pFrac => pFrac_grid, :pProd => pProd_grid, :kB5 => kB5_grid, :kD5 => kD5_grid];

    scan_parameters(scanning_set,iter_this,iter_tot,model,p_changes_fixed,scanning_parameters)
end

# Scans the phosphatase paraemters, determening which are important.
if scanning_set == "phosphatase_parameters_scan"
    model = igoshin_new_params_model
    p_changes_fixed = [:kK2 => 7.0, :η => 0.025]
    pProd_grid = lin_grid(20,150,16)
    pFrac_grid = lin_grid(10,1000,16)
    kB5_grid = lin_grid(200,5000,16)
    kD5_grid = lin_grid(1,250,16)
    scanning_parameters = [:pProd => pProd_grid, :pFrac => pFrac_grid, :kB5 => kB5_grid, :kD5 => kD5_grid];

    scan_parameters(scanning_set,iter_this,iter_tot,model,p_changes_fixed,scanning_parameters;n=200)
end

# Scans the phosphatase paraemters, determening which are important.
if scanning_set == "pProd_variation_parameters_scan"
    model = igoshin_new_params_model
    p_changes_fixed = [:kK2 => 7.0, :η => 0.025, :pFrac =>100.0]
    pProd_grid = lin_grid(20,200,181)
    scanning_parameters = [:pProd => pProd_grid];

    scan_parameters(scanning_set,iter_this,iter_tot,model,p_changes_fixed,scanning_parameters;n=200)
end

# Scans the noise parameters in the noisy phosphatase model.
if scanning_set == "phosphatase_noise_parameters_scan"
    model = noise_modulation_model
    p_changes_fixed = [:kK2 => 7.0, :ηCore => 0.025, :pFrac =>100.0]
    pProd_grid = lin_grid(20,200.0,100)
    ηAmp_grid = lin_grid(0.0,0.30,31)
    ηFreq_grid = log_grid(0.1,100.0,31) ;
    scanning_parameters = [:pProd => pProd_grid, :ηAmp => ηAmp_grid, :ηFreq=> ηFreq_grid];

    scan_parameters(scanning_set,iter_this,iter_tot,model,p_changes_fixed,scanning_parameters;n=100)
end

# Investigates how noise in the core affecte the system (relative noise in the phosphatase).
if scanning_set == "core_noise_v_phosphatase_noise_scan"
    model = noise_modulation_model
    p_changes_fixed = [:kK2 => 7.0, :pFrac =>100.0, :ηFreq => 1.0]
    pProd_grid = lin_grid(20,200.0,100)
    ηCore_grid = lin_grid(0.0,0.15,31)
    ηAmp_grid = lin_grid(0.0,0.30,31)
    scanning_parameters = [:pProd => pProd_grid, :ηCore => ηCore_grid, :ηAmp => ηAmp_grid];

    scan_parameters(scanning_set,iter_this,iter_tot,model,p_changes_fixed,scanning_parameters;n=100)
end
