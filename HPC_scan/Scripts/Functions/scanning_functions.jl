### Fetch Packages ###
using DifferentialEquations
using Serialization
using Statistics


### General Functions ###

# Makes a linnear grid.
lin_grid(start,stop,n) = [range(start,stop=stop,length=n)...]
# Makes a logarithmic grid.
log_grid(start,stop,n) = 10 .^ range(log10(start),stop=log10(stop),length=n)


### Model Handling Functions ###

# Finds the index of a parameter in a model.
get_p_idx(model::Model,par::Symbol) = findfirst(map(i -> Symbol(model.parameter_syms[i]) == par, 1:length(model.parameter_syms)))

# Generates a modified parameter set.
function modified_parameters(model,p_changes)
    p = deepcopy(model.parameters)
    foreach(pc -> p[get_p_idx(model,pc[1])] = pc[2], p_changes)
    return p
end


### Simulation Functions ###

# Monte Carlo simulation of the model.
function simulate_model(model,t_stress,l,n;p_changes=[],dt=0.00002,t_ss=100.0)
    p = modified_parameters(model,p_changes)
    u0s = solve(SDEProblem(model.system,model.u0_func(p),(0.,t_ss),p,noise_scaling=model.noises),ImplicitEM(),callback=positive_domain_cb,adaptive=false,saveat=1.,dt=dt).u[floor(Int64,t_ss/2):end]
    sprob = SDEProblem(model.system,u0s[end],(-t_stress,l),p,noise_scaling=model.noises)
    eprob = EnsembleProblem(sprob,prob_func=(p,i,r)->remake(p,u0=rand(u0s)))
    return solve(eprob,ImplicitEM(),trajectories=n,callback=CallbackSet(positive_domain_cb,model.stress_cb(0.)),adaptive=false,saveat=0.1,dt=dt,tstops=[0.])
end

# Positive domain callback.
positive_domain_cb = DiscreteCallback((u,t,integrator) -> minimum(u) < 0,integrator -> integrator.u .= integrator.uprev,save_positions = (false,false))


### Declares Scanning Functions ###

# A single function, performs the parameter scan for given input and iteration.
function scan_parameters(scanning_set,iter_this,iter_tot,model,p_changes_fixed,scanning_parameters;n=50,l=200.0,t_stress=10.0,dt=0.00002,only_print=false)
    println("Starts run for $(iter_this) of $(iter_tot)")
    println("Number of threads in use: $(Threads.nthreads())")

    parameter_syms = first.(scanning_parameters)
    parameter_grids = last.(scanning_parameters)

    parameter_combinations = vec(collect(Iterators.product(parameter_grids...)))
    divisions = [[floor(Int64,1+i*length(parameter_combinations)/iter_tot) for i in 0:(iter_tot-1)]...,length(parameter_combinations)+1]
    println("Number of iterations: $(divisions[iter_this+1]-divisions[iter_this]) out of $(length(parameter_combinations))")
    only_print && return

    println("Initiating at parameters $(parameter_combinations[divisions[iter_this]])")
    evaluations = Dict{Vector{Float64},Evaluation}();
    ss_peaks = Dict{Vector{Float64},Vector{Float64}}();
    @time for ps in parameter_combinations[divisions[iter_this]:divisions[iter_this+1]-1]
        p_changes = vcat(Pair.(parameter_syms,ps),p_changes_fixed)
        @time sols = simulate_model(model,t_stress,l,n;p_changes=p_changes,dt=dt)
        evaluations[collect(ps)] = Evaluation(model,sols)
        ss_peaks[collect(ps)] = vcat(map(i -> peak_maxes(sols[i]), 1:length(sols))...)        
    end
    save_evaluations(scanning_set,iter_this,evaluations,p_changes_fixed,parameter_syms,parameter_grids)
    save_ss_peaks(scanning_set,iter_this,ss_peaks,p_changes_fixed,parameter_syms,parameter_grids)
    println("Evaluation finished.")
end

# Goes through a scan, checks if any dataset faield to generate, and perform those scans.
function rescan_parameters(iter_org,scanning_set,iter_this,iter_tot,args...;kwargs...)
    missing_its = setdiff(1:iter_org, map(fn -> parse(Int64,fn[23:end-4]), filter(fn -> isequal(fn[1:22],"evaluations_iteration_"),readdir("../Data/$(scanning_set)/"))))
    divisions = [[floor(Int64,1+i*length(missing_its)/iter_tot) for i in 0:(iter_tot-1)]...,length(missing_its)+1]
    for missing_it in missing_its[divisions[iter_this]:divisions[iter_this+1]-1]
        scan_parameters(scanning_set,missing_it,iter_org,args...;kwargs...)
    end
end

# Structure Storing an evaluation.
struct Evaluation
    init_peaks::Vector{Float64}
    ss_peaks::Vector{Float64}
    init_peak_mean::Float64
    ss_peak_mean::Float64

    pre_stress_mean::Float64
    post_stress_mean::Float64
    post_stress_median::Float64

    post_stress_phos_means::Vector{Float64}
    post_stress_phos_stds::Vector{Float64}
    post_stress_phos_negatives::Vector{Int64}

    function Evaluation(model,sols;t_thres=5.0)
        idx_t1 = findfirst(sols[1].t .>= 0.0); idx_t2 = findfirst(sols[1].t .>= t_thres); idx_t3 = findfirst(sols[1].t .>= sols[1].t[end]/2.0);
        sigB_activities = map(sol -> getindex.(sol.u,model.sigB_idx),sols);
        phos_activities = map(pI -> vcat(map(sol -> getindex.(sol.u,pI)[idx_t3:end],sols)...), model.phos_idxs)

        init_peaks = map(sBa -> maximum(sBa[idx_t1:idx_t2]), sigB_activities)
        ss_peaks = map(sBa -> maximum(sBa[idx_t2:end]), sigB_activities)
    
        pre_stress_mean = mean(vcat(map(sol -> getindex.(sol.u[1:idx_t1],7), sols)...))
        post_stress_mean = mean(vcat(map(sol -> getindex.(sol.u[idx_t2:end],7), sols)...))
        post_stress_median = median(vcat(map(sol -> getindex.(sol.u[idx_t2:end],7), sols)...))
    
        post_stress_phos_means = map(pActs -> mean(pActs),phos_activities)
        post_stress_phos_stds = map(pActs -> std(pActs),phos_activities)
        post_stress_phos_negatives =  map(pActs -> count(pActs .< 0),phos_activities)

        new(init_peaks,ss_peaks,mean(init_peaks),mean(ss_peaks),pre_stress_mean,post_stress_mean,post_stress_median,post_stress_phos_means,post_stress_phos_stds,post_stress_phos_negatives)
    end
end

# Find the peak max values of a simulation.
function peak_maxes(sim; t_thres=2.0, p_size=1.0)
    tSSh = findfirst(sim.t .> t_thres);
    sigB_conc = getindex.(sim.u,7);    
    med15 = 1.5*median(sigB_conc[tSSh:end]);

    mask = sigB_conc .> med15;
    pass_up = findall(.!mask[1:end-1] .& mask[2:end]) .+ 1
    pass_down = findall(mask[1:end-1] .& .!mask[2:end])
    mask[1] && (length(pass_down)>1) && (pass_down = pass_down[2:end])
    !isempty(pass_up) && !isempty(pass_down) && (pass_up[end] > pass_down[end]) &&  push!(pass_down,length(mask));
    isempty(pass_down) && (length(pass_up)==1) &&  push!(pass_down,length(mask));

    idx_pulse1 = findfirst(pass_up .> tSSh)
    if idx_pulse1 != nothing
        pass_up = pass_up[idx_pulse1:end]
        pass_down = pass_down[idx_pulse1:end];
    else 
        pass_up = []; pass_down = [];
    end

    if length(pass_up) != length(pass_down)
        println(pass_up)
        println(pass_down)
    end

    pulse_idxs = filter(i -> (sim.t[pass_down[i]] - sim.t[pass_up[i]])*sum(sigB_conc[pass_up[i]:pass_down[i]] .- med15)/med15 > p_size, 1:length(pass_up))
    return map(idx -> maximum(sigB_conc[pass_up[idx]:pass_down[idx]]), pulse_idxs)
end

### Filesystem Managment ###

# Saves a set of evaluations, and some metadata.
function save_evaluations(scanning_set,iter_this,evaluations,p_changes_fixed,parameter_syms,parameter_grids)
    serialize("../Data/$(scanning_set)/evaluations_iteration_$(iter_this).jls",(evaluations=evaluations,p_changes_fixed=p_changes_fixed,parameter_syms=parameter_syms,parameter_grids=parameter_grids))
end
# Saves a set of steady state peaks, and some metadata.
function save_ss_peaks(scanning_set,iter_this,ss_peaks,p_changes_fixed,parameter_syms,parameter_grids)
    serialize("../Data/$(scanning_set)/ss_peaks_iteration_$(iter_this).jls",(ss_peaks=ss_peaks,p_changes_fixed=p_changes_fixed,parameter_syms=parameter_syms,parameter_grids=parameter_grids))
end

# Counts the number of missing files in simulation
function count_missing_files(iter_org,scanning_set)
    return iter_org - length(readdir("../Data/$(scanning_set)/"))/2    
end