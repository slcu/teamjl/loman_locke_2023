#!/bin/bash

#SBATCH -A LOCKE-SL2-CPU
#SBATCH -J igoshin_model_parameter_scan_test
#SBATCH -D /home/tel30/rds/hpc-work/projects/scan_igoshin_parameters_final/Scripts
#SBATCH -o ../Logs/parameter_scan_test.log
#SBATCH -p cclake  ### or cclake-himem
#SBATCH -c 1                   # max 32 CPUss
#SBATCH --mem-per-cpu=5980MB   # max 5980MB or 12030MB for skilake-himem
#SBATCH -t 01:00:00            # HH:MM:SS with maximum 12:00:00 for SL3 or 36:00:00 for SL2

module load julia/1.6.2
julia scan_parameters.jl 1 5000 phosphatase_noise_parameters_scan
