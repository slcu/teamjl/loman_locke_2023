# The σB alternative sigma factor circuit modulates noise to generate different types of pulsing dynamics
This repository contains all the figures of the "The σB alternative sigma factor circuit modulates noise to generate different types of pulsing dynamics" article. It also contains all scripts for generating the figures, and the simulations on which the figures are based.

Please note that the full dataset was too large to be uploded into this repository. Please read the instructions in the "Data" folder on how to manually donwload and add the full data set to this directory.


